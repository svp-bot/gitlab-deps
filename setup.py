#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="gitlab-deps",
    version="0.9",
    description="Manage Gitlab project dependencies and pipelines",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ale/gitlab-deps",
    install_requires=['python-gitlab', 'Flask', 'cheroot'],
    zip_safe=True,
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "gitlab-deps = gitlab_deps.main:main",
            ],
        },
    )

