#!/bin/sh

rev_deps_file=/var/lib/gitlab-deps/reverse-deps.list

if [ -z "$GITLAB_URL" ]; then
    echo "Gitlab-deps is not configured (GITLAB_URL empty)" >&2
    exit 0
fi

if [ ! -e $rev_deps_file ]; then
    echo "No reverse-deps.list file, executing update..." >&2
    /usr/sbin/update-gitlab-deps --no-reload
    if [ $? -gt 0 ]; then
        echo "update-gitlab-deps failed" >&2
        exit 1
    fi
fi

exec /usr/bin/gitlab-deps server \
     --token-file=$GITLAB_TOKEN_FILE --url=$GITLAB_URL \
     --addr=${ADDR:-0.0.0.0} --port=${PORT:-3535} \
     --webhook-auth-token=$WEBHOOK_AUTH_TOKEN \
     < $rev_deps_file
