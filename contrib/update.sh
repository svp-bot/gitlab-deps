#!/bin/sh

deps_file=/var/lib/gitlab-deps/deps.list
rev_deps_file=/var/lib/gitlab-deps/reverse-deps.list
reload=1
ret=0

if [ "x$1" = "x--no-reload" ]; then
    reload=0
fi

[ -e /etc/default/gitlab-deps ] && . /etc/default/gitlab-deps

if [ -z "$GITLAB_URL" -o -z "$GITLAB_TOKEN_FILE" ]; then
    echo "Gitlab-deps is not configured" >&2
    exit 2
fi

opts="--token-file=$GITLAB_TOKEN_FILE --url=$GITLAB_URL"

tmp_file="${deps_file}.tmp.$$"
trap "rm -f $tmp_file 2>/dev/null" EXIT

gitlab-deps list-projects $opts \
    | egrep "${PROJECT_REGEXP:-.*}" \
    | gitlab-deps deps $opts \
        ${GITLAB_REGISTRY_HOSTNAME:+--registry=${GITLAB_REGISTRY_HOSTNAME}} \
        > $tmp_file

if ! cmp --quiet $tmp_file $deps_file; then
    mv -f $tmp_file $deps_file
    awk '{print $2, $1}' < $deps_file > $rev_deps_file
    if [ $reload -eq 1 ]; then
        systemctl reload-or-restart gitlab-deps
        ret=$?
    fi
fi

exit $ret
