import logging


def check_hook(gl, hook_url, webhook_token, project_path, dry_run):
    project = gl.projects.get(project_path)
    found = False
    for h in project.hooks.list():
        if h.url == hook_url and h.pipeline_events:
            found = True
            break
    if found:
        return
    logging.info('adding pipeline_events hook to %s', project_path)
    if not dry_run:
        project.hooks.create({
            'url': hook_url,
            'push_events': False,
            'pipeline_events': True,
            'token': webhook_token,
        })
